namespace WebClient
{
    public sealed class Customer
    {
        #region Public Properties

        public long Id { get; init; }

        public string Firstname { get; init; }

        public string Lastname { get; init; }

        #endregion

        #region Public Methods

        public override string ToString()
        {
            return $"[ID={Id}] {Firstname} {Lastname}";
        }

        #endregion
    }
}