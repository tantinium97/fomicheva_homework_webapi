﻿using System;
using System.Threading.Tasks;
using Faker;

namespace WebClient
{
    static class Program
    {
        #region Private Field

        private static bool _isStarted;
        private static readonly Client _client = new();

        #endregion;

        #region Private Methods

        static async Task Main(string[] args)
        {
            try
            {
                _isStarted = true;
                while (_isStarted)
                {
                    switch (GetCase())
                    {
                        case "1":
                            await _client.CreateCustomerAsync(RandomCustomer());
                            break;
                        case "2":
                            await _client.GetCustomerAsync(GetId());
                            break;
                        case "3":
                            return;
                        default:
                            Console.WriteLine("Неизвестная команда. Попробуйте ввести еще раз.");
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("/nПрограмма завершена сбоем.");
                Console.WriteLine($"EM: {ex.Message}.");
                Console.WriteLine($"ST: {ex.StackTrace}.");
            }
            finally
            {
                _isStarted = false;
            }
        }

        private static CustomerCreateRequest RandomCustomer()
        {
            return new CustomerCreateRequest(Name.First(), Name.Last());
        }

        private static string GetCase()
        {
            Console.WriteLine("\nВыберите действие:" +
            "\n\t1 - Сгенерировать случайного пользователя" +
            "\n\t2 - Получить данные пользователя" +
            "\n\t3 - Выйти");
            var input = Console.ReadLine();
            return input;
        }

        private static long GetId()
        {
            Console.WriteLine("\nВведите ID пользователя");
            string input = Console.ReadLine();
            if (long.TryParse(input, out long id))
            {
                return id;
            }
            else
            {
                long newId;
                while (!long.TryParse(input, out newId))
                {
                    Console.WriteLine("Некорректное значение. ID должен быть числом. Попробуйте ввести еще раз.");
                    input = Console.ReadLine();
                }
                return newId;
            }
        }

        #endregion
    }
}