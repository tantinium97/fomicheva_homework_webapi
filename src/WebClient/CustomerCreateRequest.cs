namespace WebClient
{
    public sealed class CustomerCreateRequest
    {
        #region Public Constructors

        public CustomerCreateRequest(
            string firstName,
            string lastName)
        {
            Firstname = firstName;
            Lastname = lastName;
        }

        #endregion

        #region Public Properties

        public string Firstname { get; init; }

        public string Lastname { get; init; }

        #endregion
    }
}