﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Net.Http.Json;
using Newtonsoft.Json;

namespace WebClient
{
    public sealed class Client
    {
        #region Private Field

        private static HttpClient _httpClient;
        private const string _baseUri = "http://localhost:5000/";

        #endregion;

        #region Public Constructors

        public Client()
        {
            _httpClient = new HttpClient()
            {
                Timeout = TimeSpan.FromSeconds(15)
            };
        }

        #endregion;

        #region Public Methods

        public async Task<Customer> CreateCustomerAsync(CustomerCreateRequest customerData)
        {
            Uri requestUri = ConfigureUri("customers");
            var requestContent = JsonContent.Create(customerData);
            var response = await _httpClient.PostAsync(requestUri, requestContent);

            if (response.StatusCode == HttpStatusCode.OK)
            {
                var id = await response.Content.ReadAsStringAsync();
                Console.WriteLine("Пользователь создан.");
                var customer = await GetCustomerAsync(Convert.ToInt64(id));
                return customer;
            }
            else if (response.StatusCode == HttpStatusCode.Conflict)
            {
                Console.WriteLine("Пользовател с таким ID уже существует.");
                return null;
            }
            else
            {
                Console.WriteLine($"Ошибка создания пользователя (SC: {response.StatusCode})");
                throw new Exception("Ошибка создания пользователя");
            }
        }

        public async Task<Customer> GetCustomerAsync(long id)
        {
            Uri requestUri = ConfigureUri($"customers/{id}");
            var response = await _httpClient.GetAsync(requestUri);

            if (response.StatusCode == HttpStatusCode.OK)
            {
                var answer = await response.Content.ReadAsStringAsync();
                var customer = JsonConvert.DeserializeObject<Customer>(answer);
                Console.WriteLine("Данные пользователя:");
                Console.WriteLine(customer.ToString());
                return customer;
            }
            else if(response.StatusCode == HttpStatusCode.NotFound)
            {
                Console.WriteLine("Пользователя с таким ID не существует.");
                return null;
            }
            else
            {
                Console.WriteLine($"Ошибка получения пользователя (SC: {response.StatusCode})");
                throw new Exception("Ошибка получения пользователя");
            }
        }

        #endregion;

        #region Private Methods

        private static Uri ConfigureUri(string endpoint)
        {
            Uri uri = new(_baseUri);
            return new Uri(uri, endpoint);
        }

        #endregion;
    }
}