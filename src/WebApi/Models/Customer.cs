using System.ComponentModel.DataAnnotations;

namespace WebApi.Models
{
    public sealed class Customer
    {
        #region Public Properties

        public long Id { get; init; }

        [Required]
        public string Firstname { get; init; }

        [Required]
        public string Lastname { get; init; }

        #endregion
    }
}