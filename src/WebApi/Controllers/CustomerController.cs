using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebApi.Models;
using System.Linq;

namespace WebApi.Controllers
{
    [Route("customers")]
    public sealed class CustomerController : Controller
    {
        #region Private Field

        private readonly CustomerContext _context;

        #endregion;

        #region Public Constructors

        public CustomerController(CustomerContext context)
        {
            _context = context;
            if (!_context.Customers.Any())
            {
                var customers = new Customer[]
                {
                    new Customer{Firstname = "Tanya", Lastname="Fomicheva"},
                    new Customer{Firstname = "Sveta", Lastname="Babchuk"},
                    new Customer{Firstname = "Olya", Lastname="Nikonorova"}
                };

                context.Customers.AddRange(customers);
                context.SaveChanges();
            }
        }

        #endregion;

        #region Public Methods

        [HttpGet("{id:long}")]
        public ActionResult<Customer> GetCustomer([FromRoute] long id)
        {
            var customer = _context.Customers.SingleOrDefault(p => p.Id == id);
            if (customer is null)
                return NotFound();
            else
                return Ok(customer);
        }

        [HttpPost("")]
        public async Task<ActionResult<long>> CreateCustomerAsync([FromBody] Customer customer)
        {
            var checkCustomer = _context.Customers.SingleOrDefault(p => p.Id == customer.Id);
            if (checkCustomer is not null)
            {
                return Conflict();
            }
            else
            {
                await _context.Customers.AddAsync(customer);
                await _context.SaveChangesAsync();
                return Ok(customer.Id);
            }
        }

        #endregion;
    }
}