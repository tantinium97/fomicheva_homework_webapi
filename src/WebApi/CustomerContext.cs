﻿using Microsoft.EntityFrameworkCore;
using WebApi.Models;
using Microsoft.Extensions.Configuration;
using System.IO;

namespace WebApi;

public sealed class CustomerContext : DbContext
{
    #region Public Constructors

    public CustomerContext()
    {
        Database.EnsureCreated();
    }

    #endregion

    #region Public Properties

    public DbSet<Customer> Customers { get; set; } = null!;

    #endregion

    #region Protected Methods

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder.UseNpgsql(GetConnString());
    }

    #endregion

    #region Private Methods

    private static string GetConnString()
    {
        var confBuilder = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json");

        return confBuilder.Build().GetConnectionString("CustomerContext");
    }

    #endregion
}